FROM python:3.9
# copy src files
RUN mkdir /code
WORKDIR /code
COPY . .
# install
RUN python3.9 -m pip install -r requirements.txt
# run
ENTRYPOINT ["python3.9", "src/main.py"]

