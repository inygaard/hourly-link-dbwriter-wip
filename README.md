# hourly-link-dbwriter-wip

Concept here is fairly simple:
- Messages are received from kafka topic; each message is JSON with some metadata
  and the location of the S3 data to be written to Redshift
- Messages are buffered for a configurable amount of time to sort them. Sorting
  is performed using a heap, with the sort key given in the message metadata.
  See `kafka.consumer.queue` configuration parameters
- Messages are processed and then go into a secondary queue to commit them in
  Kafka. Messages need to be committed in the order they came from the kafka
  stream, so this sort of reverses the sorting we did previously on the way out.

# TODO

- mark processed messages for 1 day or so in Redis to handle duplicates/failed commits