""" Loads configuration from file + env vars """
import os, pyjson5, json
from pathlib import Path

env = os.environ
conf_path_raw = env.get("CGC_CONF", "dev")
conf_path = Path(conf_path_raw)
# points to file in config directory?
if conf_path.name == conf_path_raw:
	# root of project directory
	root = Path(__file__).absolute().parent.parent
	conf_path = root / "config" / (conf_path_raw+".json5")
# read config file
with conf_path.open("r") as f:
	config = pyjson5.decode_io(f)

class Config:
	""" Wraps a dict and provides cleaner interface for accessing.
		It mainly lets you do nested lookup, e.g. `conf["my.nested.var"]`
		or `conf.get("my.nested.var", default_val)`
	"""
	def __init__(self, conf):
		self._conf = conf
		self._missing = {} # used to indicate missing value
	def get(self, key, default=None):
		if isinstance(key, str):
			key = key.split(".")
		val = self._conf
		for k in key:
			if k not in val:
				return default
			val = val[k]
		return val
	def __getitem__(self, key):
		val = self.get(key, self._missing)
		if val == self._missing:
			raise KeyError(f"{key} config key is not defined")
		return val
	def __setitem__(self, key, val):
		key = key.split(".")
		last = key.pop()
		self[key][last] = val
	def __str__(self):
		# not sure how to indent w/ pyjson5
		return json.dumps(self._conf, indent=2)

config = Config(config)

# env var overrides
log_level = env.get("LOG_LEVEL")
if log_level:
	config["log.level"] = log_level
if env.get("CGC_LOCAL"):
	config["db.host_override"] = "localhost"
	config["db.port_override"] = 54329
topics = env.get("CGC_TOPIC")
if topics:
	config["kafka.consumer.topic"] = topics.split(',')
