import json, boto3, psycopg2
from time import sleep
from util import LOGGER, config

class DBConnection:
	def __init__(self):
		self.reconnect()
		
	def reconnect(self):
		""" Reconnect to db """
		self.conn = psycopg2.connect(**DBConnection.get_config())

	def execute(self, cbk, retries=-1, backoff=1000, exp_base=1.5, exp_trunc=10):
		""" Executes SQL defined in a callback function, handling reconnection/retries
			:param cbk: callback with signature (connection, cursor, attempt:int);
				attempt number starts at 1, and increments with each retry
			:param retries: int, if -1 it will retry indefinitely
			:param backoff: retry backoff in milliseconds; set to <= 0 to disable
			:param exp_base: exponential backoff base; e.g. 2 = double backoff time each retry;
				set to <= 1 to disable exponential backoff
			:param exp_trunc: max exponent for truncated exponential backoff; set to <= 0 for
				no truncation
		"""
		attempt = 0
		while True:
			attempt += 1
			try:
				with self.conn.cursor() as cursor:
					cbk(self.conn, cursor, attempt)
				break
			except Exception as e:
				LOGGER.error(f"Error running SQL query: {e}")
				# reconnect if needed
				if self.conn.closed:
					LOGGER.info("DB connection closed, reconnecting to retry")
					self.reconnect()
				else:
					# retry limit reached
					if retries >= 0 and attempt > retries:
						raise RuntimeError(f"max SQL retries ({retries}) exceeded")
					if backoff > 0:
						LOGGER.info(f"Waiting {backoff}ms to retry")
						sleep(backoff/1000)
						# exponential backoff
						if exp_base > 1 and exp_trunc >= attempt:
							backoff *= exp_base

	@classmethod
	def get_config(clazz):
		""" Fetches DB authorization config needed for querying link metadata """
		if not hasattr(clazz, "config"):
			try:
				LOGGER.info("starting DB lookup")
				session = boto3.session.Session()
				client = session.client(
					service_name='secretsmanager',
					region_name='us-east-1',
					endpoint_url='https://secretsmanager.us-east-1.amazonaws.com'
				)
				resp = client.get_secret_value(SecretId=config["db.secret"])
				secret = json.loads(resp['SecretString'])
				conf = {
					"user": secret.get('username'),
					"password": secret.get('password'),
					"dbname": secret.get('dbname'),
					"host": config.get("db.host_override", secret.get('host')),
					"port": config.get("db.port_override", secret.get('port')),
					"connect_timeout": config["db.timeout"]
				}
				# print(conf)
				if any(not v for v in conf.values()):
					LOGGER.error(f"missing DB config for {','.join(k for k,v in conf.items() if not v)}")
					raise Exception("Incomplete DB config returned from secrets manager")
				LOGGER.info(f"found credentials for {conf['user']}@{conf['host']}:{conf['port']}/{conf['dbname']} ")
				clazz.config = conf

			# For a list of exceptions thrown, see
			# https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html
			except Exception as e:
				LOGGER.exception(e)
				raise Exception(f"Error fetching database credentials") from e

		return clazz.config