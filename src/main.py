import json, heapq
from datetime import datetime
from partitions import Partitions, CommitManager
from confluent_kafka import Consumer, KafkaError
from util import LOGGER, config
from db import DBConnection

brokers = ",".join(config["kafka.brokers"])
def error_cb(err:KafkaError):
	LOGGER.error(f"Kafka error encountered, {err}")
c = Consumer({
	"bootstrap.servers": brokers,
	"group.id": config["kafka.consumer.group"],
	# this only takes effect if there are no committed messages for the group
	"auto.offset.reset": "earliest",
	"enable.auto.commit": False,
	# needed to retrieve message timestamps
	'api.version.request': True,
	"error_cb": error_cb
})
topic = config["kafka.consumer.topic"]
LOGGER.info(f"subscribing to topic: {topic}")
c.subscribe([topic])
# Topic partitions, to track timestamps/offsets
partitions = Partitions()

# whether to commit messages
COMMIT_ENABLED = config["kafka.consumer.commit"]
# Manages committing messages processed out-of-order
if COMMIT_ENABLED:
	commit_manager = CommitManager(c)

db = DBConnection()
TABLE = config["db.table"]
IAM_ROLE = config["db.iam_role"]

class Item:
	""" Sortable datum that came from kafka stream """
	def __init__(self, msg):
		# feed_id, hour_ts, bucket, key
		data = json.loads(msg.value())
		for k in data:
			setattr(self, k, data[k])
		LOGGER.info(f"Received {self.feed_id}/{self.hour_ts}")			
		self.hour_ts = datetime.strptime(self.hour_ts, "%Y-%m-%d %H:%M:%S")

		# unique id for deterministic sorting
		self.id = Item.uid
		Item.uid += 1
		# partition the message came from, for determining buffer timeout and committing
		self.partition = msg.partition()
		# message offset, for committing
		self.offset = msg.offset()
		# timestamp message was generated;
		# guaranteed to exist since already processed in Partitions class
		self.msg_ts = msg.timestamp()[1]
		# token to pass to commit manager
		if COMMIT_ENABLED:
			self.commit_token = commit_manager.add(
				f"{topic}/{self.bucket}/{self.key}",
				topic, self.partition, self.offset
			)
		else:
			self.commit_token = True

	def __lt__(self, other):
		""" Defines sort order for the message queue """
		for k in ("hour_ts", "feed_id", "id"):
			if getattr(self, k) < getattr(other, k):
				return True
		return False

	def process(self):
		""" Process this item when it is removed from the queue """
		def db_write(conn, cur, attempt):
			LOGGER.info(f"Dumping {self.key} to DB (attempt {attempt})")
			cur.execute(
				f"""COPY {TABLE}
					FROM 's3://{self.bucket}/{self.key}'
					IAM_ROLE '{IAM_ROLE}'
					FORMAT AS PARQUET
				"""
			)
			conn.commit()

		db.execute(db_write)
		LOGGER.info(f"Finished writing {self.key} to DB")
		# indicate to commit manager that this message is now processed
		if COMMIT_ENABLED:
			commit_manager.commit(self.commit_token)

Item.uid = 0
""" Counter to make Items uniquely sortable """


MIN_TIMEOUT = config["kafka.consumer.min_timeout"]
QUEUE_TIMEOUT = config["kafka.consumer.queue.timeout"]
QUEUE_LIMIT = config["kafka.consumer.queue.limit"]
# Queue for writing items to Redshift in windowed sort order
queue = []

timeout = QUEUE_TIMEOUT
while True:
	msg = c.poll(timeout=timeout)
	# new message
	if msg is not None:
		partitions.add(msg)
		# new item
		if msg.error() is None:
			item = Item(msg)
			# will be none if item was processed in the past
			if item.commit_token is not None:
				heapq.heappush(queue, item)
	else:
		partitions.elapsed(timeout)
	
	# process queue items
	min_ts = None
	while True:
		if not queue:
			# reset timeout to max
			timeout = QUEUE_TIMEOUT
			break
		top = queue[0]
		# buffer time of top expired yet?
		if len(queue) <= QUEUE_LIMIT:
			if min_ts is None:
				min_ts = partitions.min_expected()
			gap = min_ts - top.msg_ts
			# need to wait still for new messages
			if gap < QUEUE_TIMEOUT:
				timeout = max(MIN_TIMEOUT, QUEUE_TIMEOUT-gap)
				break
		# okay to process!
		heapq.heappop(queue)
		top.process()
