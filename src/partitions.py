""" Track Kafka partition offsets, timestamps, and committed messages """
import heapq
from confluent_kafka import TIMESTAMP_NOT_AVAILABLE, KafkaError, TopicPartition
from collections import defaultdict
from util import LOGGER

class Partition:
	""" Tracks last timestamp processed for each partition, and whether EOF
		has been reached on that partition
	"""
	__slots__ = ["ts", "offset", "end"]

	def __init__(self):
		# timestamp of last message received on this partition
		self.ts = None
		# offset of last message received on this partition
		self.offset = None
		# whether this partition has reached an end
		self.end = False

class Partitions:
	""" Tracks last seen timestamps for partitions of a topic. You can create multiple and use
		`update` last seen timestamps for only certain messages. This is used to detect when
		there will be no more messages for an HourBuffer
	"""
	__slots__ = ["parts", "max_ts"]

	def __init__(self):
		self.parts = {}
		# maximum timestamp of all partitions
		self.max_ts = None

	def add(self, msg) -> bool:
		""" Add a message, updating partition states
			:param msg: kafka message
			:returns: bool, whether partition timestamp or end condition has changed
		"""
		changed = False
		part_id = msg.partition()
		if part_id not in self.parts:
			self.parts[part_id] = Partition()
		part = self.parts[part_id]

		# detect end of partition
		err = msg.error()
		if err is not None:
			if err.code() == KafkaError._PARTITION_EOF:
				if not part.end:
					part.end = changed = True
			else:
				LOGGER.error(f"Kafka error encountered, {err}")
			return changed

		# regular message, not end of partition
		if part.end:
			part.end = False
			changed = True
		msg_ts_type, msg_ts = msg.timestamp()
		# TODO: could fallback to using wallclock if topic is all caught up and processing in realtime;
		#	another option is to use message offset, and assuming N ms per message, we can infer a time gap
		if msg_ts_type == TIMESTAMP_NOT_AVAILABLE:
			raise RuntimeError("need message timestamps to do buffered sorting")
		if part.ts is None or msg_ts > part.ts:
			part.ts = msg_ts
			if self.max_ts is None or msg_ts > self.max_ts:
				self.max_ts = msg_ts
			changed = True
		elif msg_ts < part.ts:
			raise RuntimeError("partition timestamps are not in ascending order")
		part.offset = msg.offset()
		return changed
	
	def elapsed(self, ms):
		""" Indicate that at least `ms` has elapsed since the last added message """
		if self.max_ts is not None:
			self.max_ts += ms
	
	def min_expected(self):
		""" Get the minimum timestamp for a next message
			:returns: timestamp, or None if no messages have come in yet
		"""
		mn = None
		for spart in self.parts.values():
			# if reached end of partition, we compare against overall message timestamps
			sts = self.max_ts if spart.end else spart.ts
			if mn is None or sts < mn:
				mn = sts
		return mn

class CommitToken:
	""" Token for marking a message for commit """
	__slots__ = ["key","group","offset","processed"]

	def __init__(self, key, group, offset):
		self.key = key
		self.group = group
		self.offset = offset
		self.processed = False
	
	def __lt__(self, other) -> bool:
		""" For sorting in commit manager; want to commit smallest offset to largest """
		return self.offset < other.offset
	
	def convert(self) -> TopicPartition:
		""" Convert to confluent_kafka structure """
		# new offset is the first unprocessed message; hence increment
		return TopicPartition(self.group, self.offset+1)

class CommitManager:
	""" Manages committing messages that have been processed in an order different from partition
		order. This manages a single consumer
	"""
	__slots__ = ["consumer","groups"]

	def __init__(self, consumer):
		# consumer is used to call commit
		self.consumer = consumer
		# holds a queue for each partition, w/ messages sorted by offset
		self.groups = defaultdict(list)

	def add(self, key:str, topic, partition, offset) -> CommitToken:
		""" Add an item (kafka message) that will need to eventually be committed
			:param key: str, globally unique key identifying this processing task
			:returns: CommitToken, can be passed to `commit` to mark the message
				as being processed; None is returned if the `key` was found to be
				processed already, so message should be skipped
		"""
		# TODO: lookup in Redis whether item has been processed
		token = CommitToken(key, (topic, partition), offset)
		queue = self.groups[token.group]
		heapq.heappush(queue, token)

	def commit(self, token: CommitToken):
		""" Mark that item can be committed now """
		# TODO: mark in Redis that item has been processed
		token.processed = True
		group = token.group
		queue = self.groups[group]
		# smallest offset seen on this topic+partition; okay to commit
		if queue[0] == token:
			while True:
				to_commit = heapq.heappop(queue)
				# last message on this topic+partition
				if not queue:
					del self.parts[group]
					break
				if not queue[0].processed:
					break
			# TODO: so could do blocking call here, then retry while there's an error;
			# 	I'm inclined to say having non-kafka dependent set of processed keys,
			# 	like in redis, is going to be better
			self.consumer.commit(offsets=[to_commit.convert()], asynchronous=True)
