""" Helper utility methods/classes """
import logging, boto3
from config import config

def configure_logger(app_name:str, ll) -> logging.Logger:
	""" Configure logger
		:param app_name: str, name of app to appear in log lines
		:param ll: desired log level
		:returns: Logger
	"""
	lg = logging.getLogger(app_name)
	ch = logging.StreamHandler()
	ch.setLevel(logging.DEBUG)
	line_format = '%(asctime)s %(name)s %(levelname)s %(funcName)s - %(message)s'
	formatter = logging.Formatter(line_format)
	ch.setFormatter(formatter)
	lg.addHandler(ch)
	lg.setLevel(ll)
	lg.info(f'log_level={lg.getEffectiveLevel()}')
	return lg

LOGGER = configure_logger(config["log.name"], config["log.level"])
LOGGER.info(f"Using config:\n{config}")
	
class AWSClient:
	""" Singleton helper class for getting AWS resource clients
		Example, to get S3: `aws.s3`
	"""
	def __init__(self):
		self.clients = {}
	def get(self, type:str):

		""" Get or create a client of a particular type """
		if type in self.clients:
			return self.clients[type]
		session_conf = config.get("aws_session")
		if session_conf is None:			
			c = boto3.resource(type)
		else:
			c = boto3.session.Session(**session_conf).resource(type)
		self.clients[type] = c
		return c
	def __getattr__(self, type):
		return self.get(type)
	def __getitem__(self, type):
		return self.get(type)

aws = AWSClient()
